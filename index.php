<?php 
require("animal.php");
require('Frog.php');
require("Ape.php");

$sheep = new Animal("shaun");
    echo "Nama Hewan : $sheep->name <br>"; // "shaun"
    echo "Legs : $sheep->legs <br>"; // 2
    echo "Cold blooded : $sheep->cold_blooded <br><br>"; // false

$sungokong = new Ape("kera sakti");
    echo "Nama Hewan : $sungokong->name <br>"; //kera sakti
    echo "Legs : $sungokong->legs <br>"; // 2
    echo "Cold blooded : $sungokong->cold_blooded <br>"; // false
    echo $sungokong->yell(); // "Auooo"
    echo "<br><br>";

$kodok = new Frog("buduk");
    echo "Nama Hewan : $kodok->name <br>"; //buduk
    echo "Legs : $kodok->legs <br>"; //2
    echo "Cold blooded : $kodok->cold_blooded <br>"; //false
    echo $kodok->jump(); //hop hop
    

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

?>